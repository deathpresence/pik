export function getRandomElements(arr, n) {
  let result = new Array(n),
    len = arr.length,
    taken = new Array(len);
  if (n > len)
    throw new RangeError("getRandom: more elements taken than available");
  while (n--) {
    let x = Math.floor(Math.random() * len);
    result[n] = arr[x in taken ? taken[x] : x];
    taken[x] = --len in taken ? taken[len] : len;
  }
  return result;
}

export function fullScreenStatus() {
  if (
    document.fullscreen ||
    document.mozFullScreen ||
    document.fullscreenElement ||
    document.msFullscreenElement ||
    document.webkitIsFullScreen
  ) {
    return true;
  } else {
    return false;
  }
}

export function getYtThumbnail(url) {
  const regExp = /.*(?:youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=)([^#\&\?]*).*/;
  const id = url.match(regExp);

  return `https://img.youtube.com/vi/${id[1]}/hqdefault.jpg`;
}

export function formatDate(date) {
  const d = new Date(date);
  const options = { day: 'numeric', month: 'numeric', year: '2-digit' }

  return d.toLocaleDateString('en-US', options);
}
