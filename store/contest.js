const setInitialState = () => ({
  contestData: null,
  step: null,
  sessionId: "",
  sessionInfo: null
});

export const state = () => setInitialState();

export const getters = {
  currentTour: state => {
    if (state.sessionInfo) {
      return Math.pow(2, state.sessionInfo.toursCount - state.sessionInfo.tour);
    } else return "";
  },
  currentStepCount: state => {
    if (state.sessionInfo) {
      return state.sessionInfo.step;
    } else return "";
  }
};

export const mutations = {
  RESET_CONTEST_STATE: state => {
    Object.assign(state, setInitialState());
  },
  SET_CONTEST_DATA: (state, payload) => {
    state.contestData = payload;
  },
  SET_STEP: (state, payload) => {
    state.step = payload;
  },
  SET_SESSION_ID: (state, payload) => {
    state.sessionId = payload;
  },
  SET_SESSION_INFO: (state, payload) => {
    state.sessionInfo = payload;
  }
};
