const setInitialState = () => ({
  appBarValue: true,
  appBarExtension: {
    typeTabs: 0,
    dateTabs: 0
  },
  preloader: false,
  signInDialog: false,
  signUpDialog: false,
  forgotPasswordDialog: false,
  editorAddDialog: false,
  editorEditDialog: {
    active: false,
    data: null
  },
  alert: {
    type: "",
    message: ""
  },
  indexPage: {
    contests: [],
    links: null,
    meta: null
  },
  contestCreatePage: {
    contestData: null,
    participants: []
  }
});

export const state = () => setInitialState();

export const getters = {
  overlay: state => {
    if (state.signInDialog || state.signUpDialog || state.forgotPasswordDialog)
      return true;
    else return false;
  },
  typeTabs: state => {
    switch (state.appBarExtension.typeTabs) {
      case 0:
        return "popular";
      case 1:
        return "newest";
    }
  },
  dateTabs: state => {
    switch (state.appBarExtension.dateTabs) {
      case 0:
        return "all";
      case 1:
        return "month";
      case 2:
        return "week";
      case 3:
        return "day";
    }
  }
};

export const mutations = {
  RESET_UI_STATE: state => {
    Object.assign(state, setInitialState());
  },
  SET_APP_BAR_VALUE: (state, payload) => {
    state.appBarValue = payload;
  },
  SET_TYPE_TABS: (state, payload) => {
    state.appBarExtension.typeTabs = payload;
  },
  SET_DATE_TABS: (state, payload) => {
    state.appBarExtension.dateTabs = payload;
  },
  SET_PRELOADER: (state, payload) => {
    state.preloader = payload;
  },
  SET_SIGN_IN_DIALOG: (state, payload) => {
    state.signInDialog = payload;
  },
  SET_SIGN_UP_DIALOG: (state, payload) => {
    state.signUpDialog = payload;
  },
  SET_FORGOT_PASSWORD_DIALOG: (state, payload) => {
    state.forgotPasswordDialog = payload;
  },
  SET_EDITOR_ADD_DIALOG: (state, payload) => {
    state.editorAddDialog = payload;
  },
  SET_EDITOR_EDIT_DIALOG: (state, payload) => {
    state.editorEditDialog = payload;
  },
  SET_ALERT: (state, payload) => {
    state.alert = payload;
  },
  SET_INDEX_PAGE: (state, payload) => {
    state.indexPage = payload;
  },
  UPDATE_INDEX_PAGE: (state, payload) => {
    state.indexPage.contests.push(...payload.contests);
    state.indexPage.links = payload.links;
    state.indexPage.meta = payload.meta;
  },
  SET_CONTEST_CREATE_PAGE_DATA: (state, payload) => {
    state.contestCreatePage.contestData = payload;
  },
  SET_CONTEST_CREATE_PAGE_PARTICIPANTS: (state, payload) => {
    state.contestCreatePage.participants = payload;
  },
  ADD_CONTEST_CREATE_PAGE_PARTICIPANT: (state, payload) => {
    state.contestCreatePage.participants.push(payload);
  },
  UPDATE_CONTEST_CREATE_PAGE_APRTICIPANT: (state, payload) => {
    const index = state.contestCreatePage.participants.findIndex(
      item => item.id === payload.id
    );
    Object.assign(state.contestCreatePage.participants[index], payload);
  },
  REMEOVE_CONTEST_CREATE_PAGE_PARTICIPANT: (state, payload) => {
    const index = state.contestCreatePage.participants.findIndex(
      item => item.id === payload
    );
    state.contestCreatePage.participants.splice(index, 1);
  }
};
