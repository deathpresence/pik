export const isMobile = {
  data() {
    return {
      isHydrated: false
    };
  },
  computed: {
    isMobile() {
      return this.isHydrated ? this.$vuetify.breakpoint.mobile : false;
    }
  },
  mounted() {
    this.isHydrated = true;
  }
};