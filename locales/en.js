const en = {
  meta: {
    description:
      "Take part and create an awesome world contests for any topics (celebrities, music, movies, video games, etc.)"
  },
  appBar: {
    menu: {
      title: "Menu",
      createContest: "Create contest",
      myContests: "My contests"
    },
    userButton: {
      profile: "Profile",
      signOut: "Sign Out"
    },
    toggle: {
      popular: "Popular",
      newest: "Newest",
      allTime: "All time",
      month: "Month",
      week: "Week",
      day: "Day"
    },
    search: {
      placeholder: "Search",
      button: "Find"
    },
    user: {
      loginButton: "Sign In",
      signInDialog: {
        title: "Sign In",
        email: "Email",
        emailRequiredRules: "Email is required",
        emailValidRules: "Email must be valid",
        password: "Password",
        passwordRules: "Password is required",
        remember: "Remember me",
        forgot: "Forgot password?",
        signIn: "Sign In",
        signUp: "Sign Up",
        alternative: "Or sign in with:",
        successSignInAlert: "Logged in successfully"
      },
      signUpDialog: {
        title: "Sign Up",
        email: "Email",
        emailRequiredRules: "Email is required",
        emailValidRules: "Email must be valid",
        username: "Username",
        usernameRules: "Username is required",
        password: "Password",
        passwordRules: "Password is required",
        repeatPassword: "Repeat password",
        repeatPasswordRules: "Passwords must match",
        signIn: "Sign In",
        signUp: "Sign Up"
      },
      forgotPasswordDialog: {
        title: "Recovery password",
        email: "Email",
        message:
          "Please, enter the email, to receive password recovery instructions",
        send: "Send"
      }
    },
    editor: {
      status: "Status",
      placeholder: "Contest not created yet",
      link: "Link",
      copy: "Copy",
      draft: "Draft",
      published: "Published",
      directLink: "Direct link",
      hidden: "Hidden",
      message:
        "Allowed number of participants for publication: 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024",
      successStatus: "Status successfully changed",
      copied: "Copied",
      addDialog: {
        title: "New participant",
        image: "Image",
        placeholder: "Pick or place file here",
        video: "Video",
        videoLink: "Link to a video",
        name: "Name",
        add: "Add",
        message:
          'Please change status to "Hidden" to be able to add participants'
      },
      editDialog: {
        title: "Edit participant",
        name: "Name",
        edit: "Edit"
      }
    }
  },
  contestItem: {
    authorTooltip: "Author",
    popularityTooltip: "Popularity",
    createDateTooltip: "Creation date",
    startTooltip: "Start",
    statsTooltip: "Statistics",
    shareTooltip: "Share"
  },
  error: {
    back: "Go back",
    404: "404 Not found",
    other: "An unknown error has occurred"
  },
  footer: {
    contests: "Contests",
    createContest: "Create contest",
    privacyPolicy: "Privacy policy",
    contactUs: "Contact us"
  },
  indexPage: {
    verifiedMessage: "Email verified",
    nothingFound: "Nothing found",
    head: {
      title: "Contests"
    }
  },
  contestPage: {
    final: "Final",
    restart: "Restart",
    zoomIn: "Fullscreen",
    zoomOut: "Zoom out",
    head: {
      title: "Contest"
    }
  },
  contestResultPage: {
    title: "Results of",
    list: "List",
    table: "Table",
    search: "Search",
    preview: "Preview",
    name: "Name",
    votes: "Number of votes",
    comments: {
      title: "Comments",
      usernameRules: "Username must contain at least 3 characters",
      messagePlaceholder: "Message",
      post: "Post"
    },
    head: {
      title: "Results of"
    }
  },
  myContestsPage: {
    title: "My contests",
    preview: "Preview",
    name: "Name",
    description: "Description",
    popularity: "Popularity",
    createContest: "Create contest",
    editContest: "Edit",
    deleteContest: "Delete",
    deleteConfirm: {
      message: "Are you sure want to delete",
      yes: "Yes",
      no: "No"
    },
    head: {
      title: "My contests"
    }
  },
  contestCreatePage: {
    hint: {
      title: "How to create and publish contest:",
      first: '1. Give name and some description, then press "Create"',
      second: '2. Then add some participants, click "Add" button for that',
      third:
        '3. After all participants added, change status at the top left corner to "Published" or other',
      dismiss: "Got it"
    },
    main: {
      title: "Main info",
      name: "Name",
      description: "Description",
      nameRule: "Enter the name",
      lengthRule: "Max length is 255 characters",
      create: "Create",
      save: "Save"
    },
    participants: {
      title: "Participants",
      preview: "Preview",
      name: "Name",
      add: "Add",
      edit: "Edit",
      delete: "Delete",
      createSuccess: "Successfully created",
      updateSuccess: "Successfully updated",
      deleteSuccess: "Participant deleted",
      confirm: {
        message: "Are you sure want to delete",
        yes: "Yes",
        no: "No"
      }
    },
    head: {
      title: "Create contest"
    }
  },
  privacyPolicyPage: {
    title: "Privacy policy",
    head: {
      title: "Privacy policy"
    },
    content:
      "<p>We built the PICK.WS app as a Free app. This SERVICE is provided by PICK.WS at no cost and is intended for use as is.</p> <p>This page is used to inform visitors regarding our policies with the collection, use, and disclosure of Personal Information if anyone decided to use our Service.</p> <p>If you choose to use our Service, then you agree to the collection and use of information in relation to this policy. The Personal Information that we collect is used for providing and improving the Service. We will not use or share your information with anyone except as described in this Privacy Policy.</p> <p>The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at PICK.WS unless otherwise defined in this Privacy Policy.</p> <p><strong>Information Collection and Use</strong></p> <p>For a better experience, while using our Service, we may require you to provide us with certain personally identifiable information, including but not limited to Email address. The information that we request will be retained by us and used as described in this privacy policy.</p> <p><strong>Log Data</strong></p> <p>We want to inform you that whenever you use our Service, in a case of an error in the app we collect data and information (through third party products) on your phone called Log Data. This Log Data may include information such as your device Internet Protocol (“IP”) address, device name, operating system version, the configuration of the app when utilizing our Service, the time and date of your use of the Service, and other statistics.</p> <p><strong>Cookies</strong></p> <p>Cookies are files with a small amount of data that are commonly used as anonymous unique identifiers. These are sent to your browser from the websites that you visit and are stored on your device's internal memory.</p> <p>This Service does not use these “cookies” explicitly. However, the app may use third party code and libraries that use “cookies” to collect information and improve their services. You have the option to either accept or refuse these cookies and know when a cookie is being sent to your device. If you choose to refuse our cookies, you may not be able to use some portions of this Service.</p> <p><strong>Service Providers</strong></p> <p>We may employ third-party companies and individuals due to the following reasons: </p> <ul><li>To facilitate our Service;</li> <li>To provide the Service on our behalf;</li> <li>To perform Service-related services; or</li> <li>To assist us in analyzing how our Service is used.</li></ul> <br/> <p>We want to inform users of this Service                  that these third parties have access to your Personal                  Information. The reason is to perform the tasks assigned to                  them on our behalf. However, they are obligated not to                  disclose or use the information for any other purpose.                </p> <p><strong>Security</strong></p> <p>                  We value your trust in providing us your                  Personal Information, thus we are striving to use commercially                  acceptable means of protecting it. But remember that no method                  of transmission over the internet, or method of electronic                  storage is 100% secure and reliable, and we cannot                  guarantee its absolute security.                </p> <p><strong>Links to Other Sites</strong></p> <p>                  This Service may contain links to other sites. If you click on                  a third-party link, you will be directed to that site. Note                  that these external sites are not operated by us.                  Therefore, we strongly advise you to review the                  Privacy Policy of these websites. We have                  no control over and assume no responsibility for the content,                  privacy policies, or practices of any third-party sites or                  services.                </p> <p><strong>Children’s Privacy</strong></p> <p>                  These Services do not address anyone under the age of 13.                  We do not knowingly collect personally                  identifiable information from children under 13. In the case                  we discover that a child under 13 has provided                  us with personal information, we immediately                  delete this from our servers. If you are a parent or guardian                  and you are aware that your child has provided us with personal information, please contact us so that we will be able to do necessary actions.</p> <p><strong>Changes to This Privacy Policy</strong></p> <p>We may update our Privacy Policy from time to time. Thus, you are advised to review this page periodically for any changes. We will notify you of any changes by posting the new Privacy Policy on this page.</p> <p><strong>Contact Us</strong></p> <p>If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us at admin@pick.ws.</p>"
  },
  contactUsPage: {
    title: 'Contact us',
    head: {
      title: "Contact us"
    },
  }
};

module.exports = en;
