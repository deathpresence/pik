const en = require('./en')
const ru = require('./ru')

const messages = {
  en,
  ru
}

module.exports = messages
