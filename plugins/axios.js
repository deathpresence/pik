import contestApi from "@/api/contest.api";
import sessionsApi from "@/api/sessions.api";
import authorizationApi from "@/api/authorization.api";
import userApi from "@/api/user.api";

export default function({ $axios }, inject) {
  inject("contest", contestApi($axios));
  inject("sessions", sessionsApi($axios));
  inject("authorization", authorizationApi($axios));
  inject("user", userApi($axios));
}
