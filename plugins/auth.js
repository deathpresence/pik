export default function({ app }) {
  app.$auth.onRedirect((to, from) => {
    console.log(to);
    return app.localePath(to);
    // you can optionally change `to` by returning a new value
  });
}
