export default ({ app }) => {
  app.head.meta.push({
    hid: "description",
    name: "description",
    content: app.i18n.t("meta.description")
  });

  app.i18n.onLanguageSwitched = () => {
    console.log(app.head.meta);
    app.head.meta[3] = {
      hid: "description",
      name: "description",
      content: app.i18n.t("meta.description")
    };
  };
};
