const path = require("path");
const fs = require("fs");
const colors = require("vuetify/es5/util/colors").default;
const messages = require("./locales");

module.exports = {
  server: {
    port: 3000, // default: 3000
    host: "127.0.0.1", // default: localhost
    https: {
      key: fs.readFileSync(path.resolve(__dirname, "server.key")),
      cert: fs.readFileSync(path.resolve(__dirname, "server.crt"))
    }
  },

  mode: "universal",

  target: "server",

  head: {
    titleTemplate: "%s | " + "PICK.WS",
    title: "PICK.WS",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "keywords",
        name: "keywords",
        content: "pick, pick.ws, piku, world contests, world cup"
      },
      {
        name: "google-site-verification",
        content: "-vrLjnKZuq0wYp1itBObqIsCPxQlil7Uw3dwpDkLSJ0"
      }
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }]
  },

  loading: {
    height: "2px",
    throttle: 0
  },

  css: ["~/assets/fonts.sass", "~/assets/main.sass"],

  plugins: [
    "~/plugins/axios",
    { src: "~plugins/analytics.js", mode: "client" },
    "~/plugins/meta"
  ],

  buildModules: [
    "@nuxtjs/eslint-module",
    "@nuxtjs/vuetify",
    [
      "nuxt-compress",
      {
        gzip: {
          cache: true
        },
        brotli: {
          threshold: 10240
        }
      }
    ]
  ],

  modules: [
    "@nuxtjs/axios",
    "@nuxtjs/auth",
    "@nuxtjs/dotenv",
    "nuxt-i18n",
    "vue-social-sharing/nuxt"
  ],

  i18n: {
    //lazy: true,
    seo: true,
    locales: [
      {
        code: "en",
        name: "English",
        iso: "en-US"
      },
      {
        code: "ru",
        name: "Русский",
        iso: "ru-RU"
      }
    ],
    defaultLocale: "en",
    detectBrowserLanguage: {
      useCookie: true
    },
    vueI18n: {
      messages
    }
  },

  axios: {
    baseURL: process.env.NUXT_ENV_API_URL,
    https: true,
    headers: {
      common: {
        Accept: "application/json"
      }
    }
  },

  auth: {
    plugins: ["~/plugins/auth.js"],
    strategies: {
      local: {
        endpoints: {
          login: {
            url: "/v1/auth/signIn",
            method: "post",
            propertyName: "data.access_token"
          },
          logout: { url: "/v1/auth/signOut", method: "post" },
          user: { url: "/v1/user", method: "get", propertyName: "data" }
        }
      }
    }
  },

  vuetify: {
    optionsPath: "./vuetify.options.js",
    customVariables: ["~/assets/variables.scss"],
    treeShake: true,
    defaultAssets: false,
    font: {
      family: "Inter"
    },
    icons: {
      iconfont: "mdi"
    },
    theme: {
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },

  features: {
    transitions: false
  },

  build: {
    extractCss: false,
    optimizeCss: true
  },

  telemetry: false
};
