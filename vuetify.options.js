import ru from "vuetify/lib/locale/ru";
import en from "vuetify/lib/locale/en";

export default function() {
  return {
    breakpoint: {
      mobileBreakpoint: 600
    },
    lang: {
      locales: {
        en,
        ru
      }
    }
  };
}
