export default $axios => ({
  async init(payload) {
    const response = await $axios.$post("/v1/sessions/init", payload);

    return response.data;
  },
  async get(payload) {
    const response = await $axios.$get(
      `/v1/sessions/step?session_id=${payload}`
    );

    if (response.success) {
      return response.data;
    } else {
      throw new Error(response.message);
    }
  },
  async step(payload) {
    const response = await $axios.$post("/v1/sessions/step", payload);

    if (response.success) {
      return response.data;
    } else {
      throw new Error(response.message);
    }
  }
});
