export default $axios => ({
  async get() {
    const response = await $axios.$get("/v1/user");

    return response.data;
  },
  async contests() {
    const response = await $axios.$get("/v1/user/contests");

    return response.data;
  },
  async verify(payload) {
    const response = await $axios.$post('/v1/user/email-confirmation/verify', payload)

    return response
  }
});
