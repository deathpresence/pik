export default $axios => ({
  async list(payload) {
    let queryParams = null;

    if (payload) {
      queryParams = payload;
    }

    const response = await $axios.$get(`/v1/contests`, {
      params: queryParams
    });

    if (response.success) {
      return response.data;
    } else {
      throw new Error(response.message);
    }
  },
  async get(payload) {
    const response = await $axios.$get(`/v1/contests/${payload}`);

    return response.data;
  },
  async getBySlug(payload) {
    const response = await $axios.$get(`/v1/contests/slug/${payload}`);

    return response.data;
  },
  async search(payload) {
    let queryParams = null;

    if (payload) {
      queryParams = payload;
    }

    const response = await $axios.$get(`/v1/main/search`, {
      params: queryParams
    });

    if (response.success) {
      return response.data;
    } else {
      throw new Error(response.message);
    }
  },
  async create(payload) {
    const response = await $axios.$post("/v1/contests", payload);

    if (response.success) {
      return response.data;
    } else {
      throw new Error(response.message);
    }
  },
  async update(payload) {
    const response = await $axios.$put(
      `/v1/contests/${payload.contestId}`,
      payload.data
    );

    if (response.success) {
      return response.data;
    } else {
      throw new Error(response.message);
    }
  },
  async delete(payload) {
    const response = await $axios.$delete(`/v1/contests/${payload}`);

    if (response.success) {
      return response.data;
    } else {
      throw new Error(response.message);
    }
  },
  async updateStatus(payload) {
    const response = await $axios.$post(
      `/v1/contests/${payload.contestId}/status`,
      payload.data
    );

    if (response.success) {
      return response.data;
    } else {
      throw new Error(response.message);
    }
  },
  async results(payload) {
    const response = await $axios.$get(`/v1/contests/slug/${payload}/results`);

    return response.data;
  },
  async addParticipant(payload) {
    const response = await $axios.$post(
      `/v1/contests/${payload.contestId}/participant`,
      payload.data
    );

    if (response.success) {
      return response.data;
    } else {
      throw new Error(response.message);
    }
  },
  async updateParticipant(payload) {
    const response = await $axios.$put(
      `/v1/participants/${payload.participantId}`,
      payload.data
    );

    if (response.success) {
      return response.data;
    } else {
      throw new Error(response.message);
    }
  },
  async deleteParticipant(payload) {
    const response = await $axios.$delete(`/v1/participants/${payload}`);

    if (response.success) {
      return response.data;
    } else {
      throw new Error(response.message);
    }
  },
  async getComments(payload) {
    const response = await $axios.$get(
      `/v1/contests/${payload.contestId}/comments`,
      {
        params: { page: payload.page || null }
      }
    );

    if (response.success) {
      return response.data;
    } else {
      throw new Error(response.message);
    }
  },
  async postComment(payload) {
    const response = await $axios.$post(
      `/v1/contests/${payload.contestId}/comments`,
      payload.data
    );

    if (response.success) {
      return response.data;
    } else {
      throw new Error(response.message);
    }
  }
});
