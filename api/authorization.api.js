export default $axios => ({
  async signIn(payload) {
    const response = await $axios.$post("/v1/auth/signIn", payload);

    return response.data;
  },
  async signUp(payload) {
    const response = await $axios.$post("/v1/auth/signUp", payload);

    return response.data;
  },
  async signOut(payload) {
    const response = await $axios.$post("/v1/auth/signOut", payload);

    return response.data;
  }
});
